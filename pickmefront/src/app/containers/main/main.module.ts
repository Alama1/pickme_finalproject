import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import {mainRoutingModule} from "./main.routing";



@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
	mainRoutingModule
  ]
})
export class MainModule { }
