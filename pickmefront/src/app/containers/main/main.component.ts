import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
	currentUser = JSON.parse(localStorage.getItem('currentUser') || '[]')

  constructor(
	  private router: Router
  ) { }

  ngOnInit(): void {
  }

  homeRoute() {
	  this.router.navigate(['/home'])
  }

  loginRoute() {
	  this.router.navigate(['/login'])
  }

	registerRoute() {
		this.router.navigate(['/registration'])
	}

	adminRoute() {
		this.router.navigate(['/admin'])
	}

	managerRoute() {
		this.router.navigate(['/manager'])
	}

}
