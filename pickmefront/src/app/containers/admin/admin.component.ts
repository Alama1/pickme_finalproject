import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

	companyName = ''
	companyEmail = ''
	representativeEmail = ''
	addCompanyResponse = ''
	getCompanyName = ''
	getCompanyRes = ''
	getUsersRes = ''
	getUserId = ''
	getUserRes = ''
	setRoleRole = ''
	setRoleEmail = ''
	setRoleRes = ''

	constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  addCompany(name: string, email: string, repEmail: string) {
	  const headers = new HttpHeaders({
		  "Access-Control-Allow-Origin": "*"
	  })
	  this.http.post('http://localhost:3000/companies',
		  {
			  		name: name,
			  		email: email,
			  		representativeEmail: repEmail
		  		},
		  { headers: headers}
	  )
		  .subscribe((data) => {
			  this.addCompanyResponse = 'Done!'
	  }, error => {
			  switch (error.status) {
				  case 400:
					  if(typeof error.error.message === "object") {
					  this.addCompanyResponse = error.error.message.join(' | ')
					  }	else {
						  this.addCompanyResponse = error.error.message
					  }
					  break
				  case 401:
					  this.addCompanyResponse = 'Authorize first!'
					  break
				  case 403:
					  this.addCompanyResponse = 'Access denied'
					  break
			  }
		  })
  }
  getCompany(name: string) {
		this.http.get(`http://localhost:3000/companies/${name}`)
			.subscribe((data) => {
				if (!data) {
					this.getCompanyRes = 'Company with this name not found!'
					return
				}
				const company = Object.entries(data);
				let representative = ''
				let compKeys = ''
				company[6][1].forEach((e: any) => {
					for (let keys in e) {
						compKeys += `${keys}: ${e[keys]}`
						compKeys += `\n`
					}
					compKeys += '\n'
				})
				for (let key in company[5][1]) {
					representative += `${key}: ${company[5][1][key]}`
					representative += '\n'
				}
				this.getCompanyRes =
					`Company id: ${company[0][1]}\n
					CompanyName: ${company[1][1]}\n
					Email: ${company[2][1]}\n
					CreatedAt: ${company[3][1]}\n
					UpdatedAt: ${company[4][1]}\n\n
					Representative: \n${representative}\n\n
					Keys: \n${compKeys}`
			}, error => {
				switch (error.status) {
				case 400:
					this.getCompanyRes = error.error.message.join('\n')
					break
				case 401:
					this.getCompanyRes = 'Authorize first!'
					break
				case 403:
					this.getCompanyRes = 'Access denied'
					break
				case 404:
					this.getCompanyRes = 'Enter company name first!'
				}
			})
  }

	getAllUsers() {
		this.http.get('http://localhost:3000/user')
			.subscribe(data => {
				let users = ''
				const newA = JSON.stringify(data)
				JSON.parse(newA).forEach((e: any) => {
					for (let keys in e) {
						users += `${keys}: ${e[keys]}`
						users += `\n`
					}
					users += '\n'
				})
				this.getUsersRes = users
			},error => {
				switch (error.status) {
					case 400:
						this.getUsersRes = 'Bad request!'
						break
					case 401:
						this.getUsersRes = 'Authorize first!'
						break
					case 403:
						this.getUsersRes = 'Access denied'
						break
				}
			})
	}

	getUserById(id: string) {
		this.http.get(`http://localhost:3000/user/${id}`)
			.subscribe(data => {
				this.getUserRes = ''
				if (!id) { this.getUserRes = 'Enter id first!'; return}
				let dataObj = JSON.parse(JSON.stringify(data))
				for(let keys in dataObj) {
					this.getUserRes += `${keys}: ${dataObj[keys]}`
					this.getUserRes += `\n`
				}
			}, error => {
				switch (error.status) {
					case 400:
						this.getUserRes = 'Bad request!'
						break
					case 401:
						this.getUserRes = 'Authorize first!'
						break
					case 403:
						this.getUserRes = 'Access denied'
						break
					case 404:
						this.getUserRes = 'No user with this id found!'

				}
			})
	}

	setRole(email: string, role: string) {
		this.http.post('http://localhost:3000/user/updateRole', { role: role.toUpperCase(), email: email })
			.subscribe(data => {
				this.setRoleRes = 'Done!'
			}, error => {
				switch (error.status) {
					case 400:
						this.setRoleRes = 'Bad request!'
						break
					case 401:
						this.setRoleRes = 'Authorize first!'
						break
					case 403:
						this.setRoleRes = 'Access denied'
						break
					case 404:
						this.setRoleRes = 'No user with this id found!'

				}
			})
	}

}
