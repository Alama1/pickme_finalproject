import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {homeRoutingModule} from "./home.routing";
import {FormsModule} from "@angular/forms";
import {GoogleMapsModule} from "@angular/google-maps";



@NgModule({
  declarations: [
    HomeComponent
  ],
    imports: [
        CommonModule,
        homeRoutingModule,
        FormsModule,
		GoogleMapsModule
    ]
})
export class HomeModule { }
