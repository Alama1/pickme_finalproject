import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../../models/User";
import {webSocketsService} from "../../modules/web-sockets/web-sockets.service";
import {GoogleMap} from "@angular/google-maps";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	currentUser = JSON.parse(localStorage.getItem('currentUser') || '[]')
	message = ''
	usersOnline = ''
	messages: string[] = [];
	driverMarker: google.maps.Marker = new google.maps.Marker({
		position: {lat: 1, lng: 1}
	})
	selfMarker: google.maps.Marker = new google.maps.Marker({
		position: {lat: 1, lng: 1}
	})

	markers = [this.selfMarker, this.driverMarker]
	interval: any;

	constructor(private webSockets: webSocketsService){}

  ngOnInit(): void {
	  this.webSockets.roomConnect();
	  const nullUser = new User()

	  nullUser.name = 'Unauthorized user'
	  nullUser.email = 'Unauthorized user'
	  nullUser.role = 'Unauthorized user'

	  this.webSockets.errorHandler()
		  .subscribe(error => {
			  if (!error) return
			  this.messages = [];
			  this.messages.push(`Authorization failed!\n message: ${JSON.parse(JSON.stringify(error)).message}`)
	  })

	  this.webSockets.getNewMessage()
		  .subscribe((message) => {
		  let messageObj = JSON.parse(JSON.stringify(message))
		  if (!messageObj) {return}
		  this.messages.push(`${messageObj.sender}: ${messageObj.text}`)
	  })

	  this.webSockets.getUserCount()
		  .subscribe((message) => {
			  if(!message) return
			  this.usersOnline = message
		  })

	  this.webSockets.getDriverCoordinates()
		  .subscribe((message) => {
			  if(!message) return
			  const driverCoords = JSON.parse(JSON.stringify(message))
			  console.log(driverCoords)
			  this.driverMarker.setValues({
				  position: { lat: driverCoords.lat, lng: driverCoords.lng }
			  })
		  })

  }

	checkUser() {
		return Boolean(localStorage.getItem('currentUser'))
	}

	@ViewChild(GoogleMap) map!: GoogleMap;
	ngAfterViewInit(){
		this.getPosition().then(r => {

			this.selfMarker.setValues({
				position: { lat: r.lat, lng: r.lng },
				map: this.map.googleMap
			})

			this.driverMarker.setValues({
				position: { lat: r.lat, lng: r.lng },
				map: this.map.googleMap
			})
			const latLng = {
				lat: r.lat,
				lng: r.lng
			}
			this.map.googleMap?.setCenter(latLng)
			this.map.googleMap?.setZoom(12)

		})
	}

	sendMessage(text: string) {

		this.messages.push(`${this.currentUser.name}: ${text}`);
		this.message = ''
		this.webSockets.sendMessage(text, this.currentUser.company_key, localStorage.getItem('currentUser') ?? '')
	}

	getPosition(): Promise<any>
	{
		return new Promise((resolve, reject) => {

			navigator.geolocation.getCurrentPosition(resp => {

					resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
				},
				err => {
					reject(err);
				});
		});

	}

	getBounds(markers: google.maps.Marker[]){
		let north;
		let south;
		let east;
		let west;

		for (const marker of markers){
			north = north !== undefined ? Math.max(north, <number>marker.getPosition()?.lat()) : <number>marker.getPosition()?.lat();
			south = south !== undefined ? Math.min(south, <number>marker.getPosition()?.lat()) : <number>marker.getPosition()?.lat();
			east = east !== undefined ? Math.max(east, <number>marker.getPosition()?.lng()) : <number>marker.getPosition()?.lng();
			west = west !== undefined ? Math.min(west, <number>marker.getPosition()?.lng()) : <number>marker.getPosition()?.lng();
		}

		north = north === undefined ? 1 : north
		south = south === undefined ? 1 : south
		east = east === undefined ? 1 : east
		west = west === undefined ? 1 : west

		return { north, south, east, west };
	}

	driverCron() {
		this.interval = setInterval(() => {
			this.getPosition().then(r => {
				this.webSockets.sendCoordinates([r.lat, r.lng])
			})
		}, 5000);
	}

	stopDriverCron() {
		clearInterval(this.interval);
	}

}
