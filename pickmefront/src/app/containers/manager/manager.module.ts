import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerComponent } from './manager.component';
import {managerRoutingModule} from "./manager.routing";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    ManagerComponent
  ],
	imports: [
		CommonModule,
		managerRoutingModule,
		FormsModule
	]
})
export class ManagerModule { }
