import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent implements OnInit {
	key = ''
	createKeyRes = ''
	userEmail = ''
	giveUserRoleRes = ''

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

	createKey(key: string) {
		this.http.post('http://localhost:3000/keys', { key: key })
			.subscribe((data) => {
				this.createKeyRes = 'Done!'
			}, error => {
				switch (error.status) {
					case 400:
						if(typeof error.error.message === "object") {
							this.createKeyRes = error.error.message.join(' | ')
						}	else {
							this.createKeyRes = error.error.message
						}
						break
					case 401:
						this.createKeyRes = 'Authorize first!'
						break
					case 403:
						this.createKeyRes = 'Access denied'
						break
				}
			})
	}

	giveDriverRole(email: string) {
		this.http.post('http://localhost:3000/user/updateDriverRole', { email: email })
			.subscribe(() => {
				this.giveUserRoleRes = 'Done!'
			}, error => {
				switch (error.status) {
					case 400:
						if(typeof error.error.message === "object") {
							this.giveUserRoleRes = error.error.message.join(' | ')
						}	else {
							this.giveUserRoleRes = error.error.message
						}
						break
					case 401:
						this.giveUserRoleRes = 'Authorize first!'
						break
					case 403:
						this.giveUserRoleRes = 'Access denied'
						break
					case 404:
						this.giveUserRoleRes = 'User with this email not found!'
						break
				}
			})
	}

}
