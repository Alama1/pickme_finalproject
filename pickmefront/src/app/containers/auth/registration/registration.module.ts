import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegistrationComponent} from "./registration.component";
import {RegistrationRouting} from "./registration.routing";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [
	  RegistrationComponent
  ],
	imports: [
		CommonModule,
		RegistrationRouting,
		FormsModule
	]
})
export class RegistrationModule { }
