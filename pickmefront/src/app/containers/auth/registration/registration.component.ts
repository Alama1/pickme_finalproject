import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
@Component({
	selector: 'app-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
	email = ''
	password = ''
	userName = ''
	registrationRes = ''

	constructor(
		private http: HttpClient,
		private router: Router
	) { }


	ngOnInit(): void {
	}

	registration(email: string, password: string, userName: string) {
		const headers = new HttpHeaders({
			"Access-Control-Allow-Origin": "*"
		})
		this.http.post('http://localhost:3000/auth/register', { email: email, password_hash: password, name: userName}, { headers: headers})
			.subscribe(obj => {
				this.registrationRes = JSON.parse(JSON.stringify(obj)).text
				this.router.navigate(['/login'])
			}, error => {
				switch (error.status){
					case 400:
						this.registrationRes = error.error.message.join('\n')
						break
					case 409:
						this.registrationRes = 'This email is already taken'
				}
			})
	}

}
