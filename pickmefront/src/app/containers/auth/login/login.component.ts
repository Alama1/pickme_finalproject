import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../../../models/User";
import {Router} from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	email = ''
	password = ''
	response: Object;

  constructor(
	  private http: HttpClient,
	  private router: Router
  ) { }


  ngOnInit(): void {
  }

  login(email: string, password: string) {
	const headers = new HttpHeaders({
		"Access-Control-Allow-Origin": "*"
	})
	this.http.post('http://localhost:3000/auth/login', { username: email, password: password}, { headers: headers} )
		.subscribe((data: any) => {
			this.response = `Success!`
			let currentUser = new User();
			currentUser.name = data.name;
			currentUser.company_key = data.company_key
			currentUser.email = data.email
			currentUser.role = data.role
			localStorage.setItem('token', data.access_token)
			localStorage.setItem('currentUser', JSON.stringify(currentUser))
			this.router.navigate(['/'])
		}, error => {
			switch (error.status){
				case 401:
					this.response = 'Login/password is not valid'
			}
		})
  }

}
