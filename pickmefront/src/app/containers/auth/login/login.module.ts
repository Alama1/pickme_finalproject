import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from "./login.component";
import {LoginRoutingModule} from "./login.routing";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [
	  LoginComponent
  ],
	imports: [
		CommonModule,
		LoginRoutingModule,
		FormsModule
	],

})
export class LoginModule { }
