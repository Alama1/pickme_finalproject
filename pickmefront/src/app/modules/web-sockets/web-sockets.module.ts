import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {webSocketsService} from "./web-sockets.service";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
	providers: [webSocketsService]
})
export class WebSocketsModule { }
