import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {io, Socket} from "socket.io-client";
import {User} from "../../models/User";


@Injectable({
	providedIn: 'root',
})
export class webSocketsService {

	public message$: BehaviorSubject<string> = new BehaviorSubject('');
	public error$: BehaviorSubject<string> = new BehaviorSubject('');
	public users$: BehaviorSubject<string> = new BehaviorSubject('');
	public coordinates$: BehaviorSubject<string> = new BehaviorSubject('');

	constructor() {
	}

	currentUser = JSON.parse(localStorage.getItem('currentUser') ?? '')

	socket = io('ws://localhost:81', {
		extraHeaders: {
			Authorization: `Bearer ${localStorage.getItem('token') ?? ''}`
		}
	});



	public sendMessage(message: string, room: string, sender: string) {
		this.socket.emit('chat', { text: message, room: room, sender: sender});
	}

	public errorHandler = () => {
		this.socket.on('Error',  message=> {
			this.error$.next(message);
		});

		return this.error$.asObservable();
	};

	public getNewMessage = () => {
		this.socket.on('chat',  message=> {
			this.message$.next(message);
		});

		return this.message$.asObservable();
	};

	public getUserCount = () => {
		this.socket.on('users',  message=> {
			this.users$.next(message);
		});

		return this.users$.asObservable();
	};

	public roomConnect = () => {
		this.socket.emit('room', {room: this.currentUser.company_key})
	}

	public getDriverCoordinates() {
		this.socket.on('coordinates',  message=> {
			this.coordinates$.next(message);
		});

		return this.coordinates$.asObservable();
	}

	public sendCoordinates(coordinates: string[]) {
		this.socket.emit('coordinates', {room: this.currentUser.company_key, lat: coordinates[0], lng: coordinates[1]})
	}
}
