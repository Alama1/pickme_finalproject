import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

	{
		path: '',
		loadChildren: () => import('./containers/main/main.module').then(m => m.MainModule)
	},
	{
		path: '',
		loadChildren: () => import('./containers/admin/admin.module').then(m => m.AdminModule),
	},
	{
		path: '',
		loadChildren: () => import('./containers/auth/login/login.module').then(m => m.LoginModule)
	},
	{
		path: '',
		loadChildren: () => import('./containers/auth/registration/registration.module').then(m => m.RegistrationModule)
	},
	{
		path: '',
		loadChildren: () => import('./containers/home/home.module').then(m => m.HomeModule)
	},
	{
		path: '',
		loadChildren: () => import('./containers/manager/manager.module').then(m => m.ManagerModule)
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
