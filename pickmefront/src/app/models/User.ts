export class User {
  email: string;
  name: string;
  role: string;
  company_key: string;
}
