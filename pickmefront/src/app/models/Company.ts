export class Company {
	id: number;
	companyName: string;
	email: string;
	createdAt: Date;
	updatedAt: Date;
	representative: string;
	keys: string[];
}
