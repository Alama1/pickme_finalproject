# Pickmefront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.4.

## Installation

First you need to run `npm install` to install node_modules. After installation is done, run `npm start` 
to start your server!

## Description

This project is designed to present possibilities of backend [PickMe](https://gitlab.com/Alama1/pickme_finalproject/-/tree/dev) server
there is 6 routes you can use default one `/` signIn `/login` signUp `/register` home `/home` admin panel `/admin` manager panel `/manager`

## Documentation

To register you need to `/register` route and use your unique email (5-64 symbols), full name and password (5-64 symbols)
to signUp. Then you can signIn with your new account

After registration is complete, use `/login` route to signIn with your account.

`For companies:` contact server administrator to create new company and set manager role to your account. 
If manager rights are received you can access `/manager` route to maintain your company.
`Attention! You need to be signed with you manager account to access server endpoints!`

`For users:` After you successfully registered your account, ask your company manager to set you your key, and then you can access all
features.

`For drivers`: You need to do the same as for users, plus ask managers to give you a driver role. After you signIn with your driver account 
and ready to drive just open `/home` route and press `Start sending coordinates` button! At the end of the trip don't forget to stop it with another button.

## Authors
Main and only
developer Kuzhdenko Maksim [gitLab](https://gitlab.com/Alama1)
