import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CompanyRepository } from './company.repository';
import { UserService } from '../user/user.service';
import { UserRoleEnum } from '../enums/users/roles';
import { Company } from './company.entity';
import { CompanyRequestDto } from '../dtos/company/company.request.dto';
import { User } from '../user/user.entity';

@Injectable()
export class CompanyService {
    constructor(
    @InjectRepository(CompanyRepository)
    private companiesRepository: CompanyRepository,
    private userService: UserService,
    ) {}

    async addCompany(company: CompanyRequestDto) {
        const newCompany = new Company();
        let user;
        try {
            user = this.userService.getUserByEmail(company.representativeEmail);
        } catch (e) {
            return e.code;
        }
        await this.userService.updateUserRole(
            company.representativeEmail,
            UserRoleEnum.MANAGER,
        );
        newCompany.email = company.email;
        newCompany.Representative = await user;
        newCompany.companyName = company.name;
        return this.companiesRepository.save(newCompany);
    }

    async getCompanyByName(companyName: string) {
        return this.companiesRepository.findOne({
            where: { companyName: companyName },
            relations: ['Representative', 'keys'],
        });
    }

    async getCompanyByRepresentative(user: User) {
        return this.companiesRepository.findOne({
            relations: ['Representative'],
            where: { Representative: user }
        });
    }
}
