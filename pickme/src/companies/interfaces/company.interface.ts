import { User } from '../../user/user.entity';
import { Key } from '../keys/key.entity';

export interface CompanyInterface {
  id?: number;
  companyName: string;
  email: string;
  Representative: User;
  keys?: Key[];
  createdAt: Date;
  updatedAt: Date;
}
