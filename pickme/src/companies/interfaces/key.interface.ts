import { Company } from '../company.entity';

export interface KeyInterface {
  id?: number;
  key: string;
  company: Company;
  createdAt: Date;
  updatedAt: Date;
}
