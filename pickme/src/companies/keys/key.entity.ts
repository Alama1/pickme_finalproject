import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
} from 'typeorm';
import { KeyInterface } from '../interfaces/key.interface';
import { Company } from '../company.entity';

@Entity()
export class Key implements KeyInterface {
  @PrimaryGeneratedColumn()
      id: number;

  @ManyToOne(() => Company, (company) => company.keys)
      company: Company;

  @Column()
      key: string;

  @CreateDateColumn({ update: false })
      createdAt: Date;

  @UpdateDateColumn({ update: true })
      updatedAt: Date;
}
