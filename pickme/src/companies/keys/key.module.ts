import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeysService } from './keys.service';
import { KeyRepository } from './key.repository';

@Module({
    imports: [TypeOrmModule.forFeature([KeyRepository])
    ],
    providers: [
        KeysService,
    ],
    exports: [KeysService],
})
export class KeyModule {
}
