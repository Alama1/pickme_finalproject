import { EntityRepository, Repository } from 'typeorm';
import { Key } from './key.entity';

@EntityRepository(Key)
export class KeyRepository extends Repository<Key> {
    findById(keyId: string): Promise<Key> {
        return this.findOne({
            where: { id: keyId },
        });
    }
}
