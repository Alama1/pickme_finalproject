import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { KeyRepository } from './key.repository';
import { Company } from '../company.entity';

@Injectable()
export class KeysService {
    constructor(
    @InjectRepository(KeyRepository)
    private keyRepository: KeyRepository,
    ) {}

    async getAllKeys() {
        return this.keyRepository.find();
    }

    async createKey(key: string, company: Company) {
        return this.keyRepository.save({
            company: company,
            key: key
        })
    }

    async deleteKey(keyId: number) {
        return this.keyRepository.delete(keyId);
    }
}
