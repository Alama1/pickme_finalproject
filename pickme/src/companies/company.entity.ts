import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    Unique,
    OneToOne,
    JoinColumn,
    OneToMany,
} from 'typeorm';

import { CompanyInterface } from './interfaces/company.interface';
import { User } from '../user/user.entity';
import { Key } from './keys/key.entity';

@Entity()
@Unique('email', ['email'])
@Unique('companyName', ['companyName'])
export class Company implements CompanyInterface {
  @PrimaryGeneratedColumn()
      id: number;

  @Column({ type: 'varchar', length: 32, nullable: false, unique: false })
      companyName: string;

  @Column({ type: 'varchar', length: 100, nullable: false })
      email: string;

  @OneToOne(() => User)
  @JoinColumn()
      Representative: User;

  @OneToMany(() => Key, (key) => key.company)
      keys?: Key[];

  @CreateDateColumn({ update: false })
      createdAt: Date;

  @UpdateDateColumn({ update: true })
      updatedAt: Date;
}
