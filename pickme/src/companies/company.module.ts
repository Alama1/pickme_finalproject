import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompanyService } from './company.service';
import { KeyModule } from './keys/key.module';
import { CompanyRepository } from './company.repository';
import { UserModule } from '../user/user.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([CompanyRepository]),
        KeyModule,
        UserModule,
    ],
    providers: [CompanyService],
    exports: [CompanyService],
})
export class CompanyModule {}
