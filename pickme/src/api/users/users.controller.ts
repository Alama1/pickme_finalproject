import {
    Get,
    Post,
    Body,
    Param,
    Controller,
    ConflictException,
    BadRequestException,
    NotFoundException,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';

import { UserService } from '../../user/user.service';
import { UsersResponseDto } from '../../dtos/user/user.response.dto';
import { UserUpdateKeyDto } from '../../dtos/user/user.updateKey.dto';
import { UserUpdateRoleDto } from '../../dtos/user/user.updateRole.dto';
import { Roles } from '../../decorators/roles.decorator';
import { UserUpdateDriverDto } from '../../dtos/user/user.updateDriver.dto';
import { UserRoleEnum } from '../../enums/users/roles';

@Controller('user')
export class UsersController {
    constructor(private userService: UserService) {
    }

    @Roles('ADMIN')
    @Get()
    async findAll(): Promise<UsersResponseDto[]> {
        try {
            const users = await this.userService.getAllUsers();
            return users.map((user) => new UsersResponseDto(user));
        } catch (e) {
            throw new BadRequestException();
        }
    }

    @Roles('ADMIN')
    @Get(':userId')
    async findOne(@Param('userId') id: number): Promise<UsersResponseDto> {
        let user;
        try {
            user = await this.userService.getUserById(id);
        } catch (e) {
            throw new BadRequestException();
        }
        if (!user) throw new NotFoundException();
        return user;
    }

    @Roles('admin', 'manager')
    @Post('updateKey')
    @UsePipes(new ValidationPipe())
    async updateCompanyKey(
        @Body() data: UserUpdateKeyDto,
    ): Promise<UsersResponseDto> {
        try {
            const { email, key } = data;
            const user = await this.userService.updateCompanyKey(email, key);
            return new UsersResponseDto(user);
        } catch (error) {
            throw BadRequestException;
        }
    }

    @Roles('admin', 'manager')
    @Post('updateDriverRole')
    @UsePipes(new ValidationPipe())
    async updateDriverRole(@Body() data: UserUpdateDriverDto): Promise<UsersResponseDto> {
        let user;
        try {
            user = await this.userService.getUserByEmail(data.email);
        } catch (e) {
            throw new NotFoundException();
        }
        if (!user) throw new NotFoundException();

        if (user.role === 'ADMIN' || user.role === 'MANAGER') {
            throw new BadRequestException('You can\'t update role of that user!');
        }
        return new UsersResponseDto(
            await this.userService.updateUserRole(user.email, UserRoleEnum.DRIVER),
        );
    }

    @Roles('admin')
    @Post('updateRole')
    @UsePipes(new ValidationPipe())
    async updateRole(@Body() data: UserUpdateRoleDto): Promise<UsersResponseDto> {
        try {
            const user = await this.userService.updateUserRole(data.email, data.role);
            return new UsersResponseDto(user);
        } catch (error) {
            if (error.code === 'ER_WARN_DATA_TRUNCATED') {
                throw new ConflictException('User role is not valid');
            } else {
                throw new BadRequestException();
            }
        }
    }
}
