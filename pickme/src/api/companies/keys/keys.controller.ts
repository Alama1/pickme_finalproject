import { Body, Controller, Post, UsePipes, ValidationPipe, Headers, BadRequestException } from '@nestjs/common';
import { KeysService } from '../../../companies/keys/keys.service';
import { CompanyKeyDto } from '../../../dtos/company/company.key.dto';
import { Roles } from '../../../decorators/roles.decorator';
import { JwtService } from '@nestjs/jwt';
import { CompanyService } from '../../../companies/company.service';
import { UserService } from '../../../user/user.service';

@Controller('keys')
export class KeysController {
    constructor(
    private keyService: KeysService,
        private jwtService: JwtService,
        private companyService: CompanyService,
        private userService: UserService
    ) {}

  @Post()
  @Roles('ADMIN', 'MANAGER')
  @UsePipes(new ValidationPipe())
  async createKey(@Headers('authorization') header, @Body() body: CompanyKeyDto) {
      const JWTUser = this.jwtService.verify(header.split(' ')[1])
      const user = await this.userService.getUserByEmail(JWTUser.email);
      const company = await this.companyService.getCompanyByRepresentative(user)
      if (!company) throw new BadRequestException('We could not find a company where you represent it')
      return this.keyService.createKey(body.key, company)
    }
}
