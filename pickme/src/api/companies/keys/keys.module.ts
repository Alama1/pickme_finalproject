import { Module } from '@nestjs/common';
import { KeyModule } from '../../../companies/keys/key.module';
import { KeysController } from './keys.controller';
import { CompanyModule } from '../../../companies/company.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../../auth/constants';
import { UserModule } from '../../../user/user.module';

@Module({
    imports: [
        KeyModule,
        CompanyModule,
        UserModule,
        JwtModule.register({ secret: jwtConstants.secret }),],
    controllers: [KeysController],
})
export class KeysModule {}
