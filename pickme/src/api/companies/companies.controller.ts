import {
    BadRequestException,
    Body,
    ConflictException,
    Controller,
    Get,
    Param,
    Post,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';
import { CompanyService } from '../../companies/company.service';
import { CompanyRequestDto } from '../../dtos/company/company.request.dto';
import { Roles } from '../../decorators/roles.decorator';
import { companyGetDto } from '../../dtos/company/company.get.dto';

@Controller('companies')
export class CompaniesController {
    constructor(private companyService: CompanyService) {}

  @Roles('ADMIN')
  @Post()
  @UsePipes(new ValidationPipe())
    async addCompany(@Body() body: CompanyRequestDto) {
        try {
            return await this.companyService.addCompany(body);
        } catch (error) {
            if (error.code === 'ER_DUP_ENTRY') {
                console.log(error.code);
                throw new ConflictException(
                    'Company with this email/name already exists',
                );
            } else if (
                error.message.startsWith(
                    'Could not find any entity of type "User" matching:',
                )
            ) {
                throw new BadRequestException('No user with this email found');
            } else {
                throw new BadRequestException();
            }
        }
    }

  @Roles('ADMIN')
  @Get(':name')
  async getCompanyInfo(@Param() name: companyGetDto) {
      return await this.companyService.getCompanyByName(name.name);
  }
}
