import { Module } from '@nestjs/common';
import { CompaniesController } from './companies.controller';
import { CompanyModule } from '../../companies/company.module';
import { KeysModule } from './keys/keys.module';

@Module({
    imports: [CompanyModule, KeysModule],
    controllers: [CompaniesController],
})
export class CompaniesModule {}
