import { UserRoleEnum } from '../../enums/users/roles';

export interface UserInterface {
  id?: number;
  name: string;
  email: string;
  password_hash: string;
  role: UserRoleEnum;
  company_key?: string;
  createdAt: Date;
  updatedAt: Date;
}
