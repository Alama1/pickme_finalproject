import { Module } from '@nestjs/common';
import { KeysRemoverCron } from './expiredKeysRemover.cron';
import { KeyModule } from '../../companies/keys/key.module';
import { LoggerModule } from '../logger/logger.module';

@Module({
    imports: [KeyModule, LoggerModule],
    providers: [KeysRemoverCron],
    exports: [KeysRemoverCron],
})
export class CronModule {}
