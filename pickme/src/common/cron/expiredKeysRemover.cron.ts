import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { KeysService } from '../../companies/keys/keys.service';
import { LoggerService } from '../logger/logger.service';

@Injectable()
export class KeysRemoverCron {
    constructor(
    private keysService: KeysService,
    private logger: LoggerService,
    ) {}

  //Ones a day check
  @Cron('0 0 * * *')
    async handleCron() {
        let deletedKeysCount = 0;
        const deletedKeys = '';
        const allKeys = this.keysService.getAllKeys();
        const dateNow = new Date();
        allKeys.then((keys) => {
            keys.forEach((key) => {
                const keyWithDuration = key.createdAt.setMonth(
                    key.createdAt.getMonth() + parseInt(process.env.COMPANYKEYDURATION),
                );
                if (keyWithDuration < dateNow.getTime()) {
                    deletedKeysCount += 1;
                    this.keysService.deleteKey(key.id);
                    deletedKeys.concat(
                        'KEY: ',
                        key.key,
                        'COMPANY: ',
                        key.company.companyName,
                    );
                }
            });
        });
        const logText = `CRON: Deleting expired keys... ${deletedKeysCount} were deleted. ${deletedKeys}`;
        this.logger.createLog(logText);
    }
}
