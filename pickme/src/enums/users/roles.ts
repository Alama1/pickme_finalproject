export enum UserRoleEnum {
  USER = 'USER',
  DRIVER = 'DRIVER',
  MANAGER = 'MANAGER',
  ADMIN = 'ADMIN',
}
