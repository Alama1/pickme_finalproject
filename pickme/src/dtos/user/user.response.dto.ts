import { UserInterface } from '../../user/interfaces/user.interface';
import { UserRoleEnum } from '../../enums/users/roles';

export class UsersResponseDto {
    readonly id?: number;

    readonly name: string;

    readonly email: string;

    readonly password_hash: string;

    readonly role: UserRoleEnum;

    readonly company_key?: string;

    readonly createdAt: Date;

    readonly updatedAt: Date;

    constructor(data?: Partial<UserInterface>) {
        if (data) {
            this.id = data.id;
            this.role = data.role;
            this.name = data.name;
            this.email = data.email;
            this.createdAt = data.createdAt;
            this.company_key = data.company_key;
        }
    }
}
