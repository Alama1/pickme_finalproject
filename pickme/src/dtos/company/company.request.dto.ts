import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class CompanyRequestDto {
  @IsString()
  @IsNotEmpty()
  @Length(1, 64, { message: 'name must be between 1 and 64 characters' })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @Length(5, 64, { message: 'email must be between 5 and 64 characters' })
  readonly email: string;

  @IsNotEmpty()
  @IsEmail()
  readonly representativeEmail: string;
}
