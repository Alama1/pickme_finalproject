import { IsNotEmpty, IsString } from 'class-validator';

export class CompanyKeyDto {
  @IsString()
  @IsNotEmpty()
  readonly key: string;
}
