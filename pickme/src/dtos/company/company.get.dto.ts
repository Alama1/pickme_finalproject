import { IsNotEmpty, IsString } from 'class-validator';

export class companyGetDto {
  @IsString()
  @IsNotEmpty()
  readonly name: string;
}
